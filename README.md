# Advanced Shell Scripting 2023

Školení pokročilého skriptování v shellu.

## Správa certifikátů

> - `pem`: Privacy Enhanced Mail (txt)
> - `key`: PEM (RSA) Private Key
> - `csr`: PEM Certificate Signing Request
> - `crt`: PEM Certificate
> - `CA`: Certification Authority

- Generování self-signed certifikátu
```
openssl req -new -x509 -nodes -out server.crt -keyout server.key -days 365
```
- Dávkové generování
```
openssl req -new -x509 -nodes -out server.crt -keyout server.key -days 365 \
 -subj "/C=CZ/ST=Prague/L=Prague/O=MyCorporation/OU=Web/"\
"CN=www.mycorp.net/emailAddress=admin@mycorp.net/"
```
- Zobrazení informací z certifikátu
```
openssl x509 -noout -text -in server.crt
```
- Vytvoření CA
```
openssl genrsa -out CA.key 2048
openssl req -x509 -new -nodes -days 7300 -key CA.key -days 7300 -out CA.pem
```
- Vytvoření žádosti o certifikát
```
openssl genrsa -out alice.key 2048
openssl req -new -key alice.key -out alice.csr
```
- Zobrazení informací z žádosti
```
openssl req -noout -text -in alice.csr
```
- Podepsání žádosti CA
```
openssl x509 -sha256 -req -in alice.csr -out alice.crt \
 -CA CA.pem -CAkey CA.key -CAcreateserial -days 1095
```


## Správa klíčů pro SSH

- Konfigurace SSH klíčů serveru `/etc/ssh/sshd_config`
```
HostKey /etc/ssh/ssh_host_rsa_key (práva!)
PermitRootLogin prohibit-password
```
- Uživatelské klíče
```
ssh-keygen
```
- Uživatelská konfigurace v `~/.ssh/config`
```
Host *
	ServerAliveInterval 120
```
```
Host fray1
	Hostname fray1.fit.cvut.cz
	PubkeyAcceptedKeyTypes=+ssh-dss
	HostKeyAlgorithms=+ssh-dss
	StrictHostKeyChecking no
	Port 22
```
```
Host 192.168.1.*
	Port 22
	CheckHostIP no
	StrictHostKeyChecking no
	PasswordAuthentication yes
	UserKnownHostsFile /dev/null
	User osboxes
```
```
Host ...
	Hostname ...
	Port 22
	IdentityFile ~/.ssh/...
	IdentitiesOnly yes
	StrictHostKeyChecking yes
```

## GPG

- Vytvoření a vylistování klíčů
```
gpg --gen-key
gpg --list-keys
```
- Import/export klíčů
```
gpg --export --output alice.pub alice                 # PUB-BINARY
gpg --export --armor --output alice.pub alice         # PUB-ASCII
```
```
gpg --export-secret-keys --output alice alice         # PRIV-BINARY
gpg --export-secret-keys --armor --output alice alice # PRIV-ASCII
```
```
gpg --import alice.pub
gpg --import alice
```
- Šifrování/dešifrování
```
gpg --encrypt --recipient bob doc
gpg --decrypt doc.gpg
```
- Podepisování/ověřování
```
gpg --sign doc
gpg --verify doc.gpg
gpg --decrypt doc.gpg
```
- Podepsání/zašifrování a rozšifrování/ověření
```
gpg --sign --output - doc | gpg --encrypt --recipient bob - > doc.gpg
gpg --decrypt doc.gpg | gpg --decrypt > doc
gpg --decrypt doc.gpg | gpg --verify
```


https://gnupg.org/gph/en/manual.html

## Bezpečné použití shellu/skriptování

- Použití `" "` a `' '` u expanzí
- Ošetřování vstupů (kontrola), ukázka skriptu a zpracování přepínačů
- Nepoužívání nedefinovaných proměnných (výchozí hodnoty), `set -u`/`set -o unset`
- Kontrola návratových kódů *(příklad s backup)*
  - `set -e`/`set -o errexit`
  - `set -o pipefail`
  - `$PIPESTATUS[]`
- Nástroj `shellcheck`
- Nezadávání hesla jako parametrů příkazů, ale načítat je ze souboru (nastavení a kontrola práv)
- Nepovolení přepisování souborů (`set -C`/`set -o noclobber`)
- Ignorování `CTRL+D`
  - `set -o ignoreeof`
  - `IGNOREEOF=2`
- Debug
  - `set -v`/`set -o verbose`
  - `set -x`/`set -o xtrace`
- Použití polí místo skalárů pro více položek, nastavení `$IFS`
- Vytváření dočasných souborů/adresářů pomocí `mktemp`


## Práce s historií

- Přidávání na konec historie místo přepisu: `shopt -s histappend`
- Vyhledávání v historii pomocí `CTRL+R`
- Editace předchozí řádky v editoru příkazem `fc` (fix-command) podle `$FCEDIT`, `$EDITOR`, pak `vi`
- `fc` *od do* pro zopakování rozsahu řádků nebo pro vytvoření skriptu z části historie
- Pokračování v předešlém řádku (jeho rozšíření), např.
```
grep -i permitrootlogin /etc/ssh/sshd_config
!! | wc -l
!! > file
```
- Neukládání historie některých příkazů
```
export HISTCONTROL='ignorespace:ignoredups'
export HISTIGNORE='ll:ls:ls *:pwd:&'
```
- Přenos historie mezi shelly (terminály).  
  *Pozor, má i své nevýhody - rozbíjí posloupnost příkazů (logickou návaznost).*
  1. Okamžité uložení (append)
  1. Vyprázdnění historie (clear)
  1. Načte a připojí historii ze souboru (read)
```
export PROMPT_COMMAND="history -a; history -c; history -r"
```


## Nastavení shellu pro pohodlnější používání

### Práce s adresáři

- Automatické `cd`, pokud je zadán pouze adresář
```
shopt -s autocd
/tmp
```
- Oprava překlepů `cd`
```
shopt -s cdspell
cd /tpm
```
- Oprava překlepu `dc`/`cd`
```
alias dc=cd
```
- Expanze adresářů při doplňování
```
shopt -s direxpand
foo=/var/tmp
ls $foo <TAB>
```
- Hledání adresářů
```
CDPATH=~/Project:/var/www/vhosts
cd foo
```
- Zkrácení počtu adresářů v promptu
```
PROMPT_DIRTRIM=2
```

### Nastavení vstupu z příkazové řádky v `~/.inputrc`

- Hot-keys
```
"\C-o": "> /dev/null"
"\ed": "declare -p "
```
- Skok po slovech na řádce
```
"\e[1;5D": backward-word
"\e[1;5C": forward-word
```
- Pohyb v historii (zpět) místo `CTRL+S`
```
"\C-t": forward-search-history
```
- Blikání párových závorek a expanze tildy
```
set blink-matching-paren on
set expand-tilde on
```
- Zakomentování řádku (odkudkoliv)
```
"##": insert-comment
```

## Filtry

### Primitivní textové filtry pro práci s daty

- Selekce: `grep`
- Projekce: `cut`
- Řazení: `sort`
- Spojení: `cat`, `paste`
- Přirozené spojení: `join`
- Unikátnost/duplicity: `uniq`, `comm`


### Pokročilé filtry pro práci se (strukturovanými) daty

- Náhrady, textové manipulace: `sed`
- Výpočty, práce s tabulkami a poli: `awk`
- Manipulace s JSON daty: `jq`
- Manipulace s YAML daty: `yq`
- Manipulace s XML daty: `xmlstarlet`


## Zpracování přepínačů a argumentů

- Zpracování krátkých přepínačů (vestavěné)
```
./my_script.sh -v -o out.ps -b in1.pdf in2.pdf
       $0      $1 $2   $3   $4    $5      $6      $#=6
```
```
while getopts vo:b opt; do
  case $opt in
    v) VERBOSE=1;;
    o) OUTPUT="$OPTARG";;
    b) BORDER=1;;
    \?) echo "$USAGE" >&2; exit 2;;
  esac
done
shift $(( OPTIND - 1 ))
```
- Zpracování dlouhých přepínačů
```
LINE=$( getopt \
          -n "$0" \
          -o lm:o::x: \
          -l long,mandatory:,optional::,multiple: \
          -- "$@" )

 eval set -- "$LINE"

 while (( $# > 0 )); do
	case $1 in
		-l|--long) LONG=1; shift;;
		-m|--mandatory) MANDATORY=$2; shift 2;;
		-o|--optional) OPTIONAL=$2; shift 2;;
		-x|--multiple) MULTIPLE+=("$2"); shift 2;;
		--) shift; break;;
		 *) echo "$0: Unrecognised option '$1'" >&2; usage >&2; exit 2;;
	esac
done
```
- Zpracování argumentů
```
./my_script.sh in1.pdf in2.pdf
       $0         $1      $2      $#=2
```
```
while [ $# -gt 0 ]; do
  process "$1"
  shift
done
```
```
for arg in "$@"; do
  process "$arg"
done

```
