#!/bin/bash
DEBUG=0
LONG=0
MANDATORY=''
OPTIONAL=''
MULTIPLE=()
: ${COLUMNS:=$(tput cols)}

hr() { printf -v HR '%*s\n' "$COLUMNS" ' '; echo ${HR// /-}; }

pdump() {
	((DEBUG)) || return
	n=1
	for i
	do
		printf "%d:\t'%s'\n" $((n++)) "$i"
	done
	hr
}

usage() {
	echo "Usage:  $0 [OPTIONS...] argument...

        -l|--long            option without argument
        -m|--mandatory arg   option with single argument
        -o|--optional arg    option with optional argument
        -x|--multiple arg    option that can be specified multiple times"
}

pdump "$@"

set -x
if ! LINE=$(
	getopt -n "$0" \
          -o lm:o::x: \
          -l long,mandatory:,optional::,multiple: \
          -- "$@"
)
then
   set +x
   usage >&2
   exit 2
fi
eval set -- "$LINE"
set +x
hr

pdump "$@"

while [ $# -gt 0 ]
do
	case $1 in
		-l|--long) LONG=1; shift;;
		-m|--mandatory) MANDATORY=$2; shift 2;;
		-o|--optional) OPTIONAL=$2; shift 2;;
		-x|--multiple) MULTIPLE+=("$2"); shift 2;;
		--) shift; break;;
		 *) echo "$0: Unrecognised option '$1'" >&2; usage >&2; exit 2;;
	esac
done

printf "%10s: '%s'\n" \
	long "$LONG" \
	mandatory "$MANDATORY" \
	optional "$OPTIONAL"

((${#MULTIPLE[*]})) && printf "  multiple: '%s'\n" "${MULTIPLE[@]}"

(($#)) && printf "  argument: '%s'\n" "$@"

hr
